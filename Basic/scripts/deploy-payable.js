
//const hre = require("hardhat");
const { ethers, waffle} = require("hardhat");
const provider = waffle.provider;

async function deployContract(cName, initialBalance ) {
    const Contract = await ethers.getContractFactory(cName);
    const contract = await Contract.deploy({ value: initialBalance });
    await contract.deployed();
    console.log("Deploy: ", cName);
    return contract;
}


async function main() {
    [owner, addr1, addr2, ...addrs] = await ethers.getSigners();

    const initialBalance = ethers.utils.parseEther("1");
    const Payable = await deployContract("PayableContract", initialBalance );

    /*console.log('Owner balance: ', await provider.getBalance(owner.address));
    console.log('Contract balance: ', await provider.getBalance(Payable.address));

    await owner.sendTransaction({
        to: Payable.address,
        value: ethers.utils.parseEther("1"), // 1 
        gas
    });
    
    console.log(await provider.getBalance(Payable.address));*/

}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
