// SPDX-License-Identifier: MIT
// compiler version must be greater than or equal to 0.8.13 and less than 0.9.0
pragma solidity >=0.6.0 <0.9.0;
import "hardhat/console.sol";

contract HelloWorld {
    string public greet = "Hello World!";
    
    function PrintMsg() public view {
        console.log("Text2: ", greet);
    }
}