// SPDX-License-Identifier: MIT
pragma solidity >=0.6.0 <0.9.0;
import "hardhat/console.sol";

contract Mapping {
    // Mapping from address to uint
    mapping(address => uint) public myMap;
    mapping(address => string) public myMapStr;
    mapping(address => mapping(uint => bool)) public nested;
    mapping(address => uint[]) public nested2;

    function get(address _addr) public view returns (uint) {
        // Mapping always returns a value.
        // If the value was never set, it will return the default value.
        console.log("get.address: ",_addr);
        return myMap[_addr];
    }

    function set(address _addr, uint _i) public {
        // Update the value at this address
        console.log("set.address: ",_addr);
        console.log("set.i: ",_i);
        myMap[_addr] = _i;
    }

    function remove(address _addr) public {
        // Reset the value to the default value.
        console.log("remove.address: ",_addr);
        delete myMap[_addr];
    }

    
    function getStr(address _addr) public view returns (string memory) {
        // Mapping always returns a value.
        // If the value was never set, it will return the default value.
        console.log("get.address: ",_addr);
        return myMapStr[_addr];
    }

    function setStr(address _addr, string memory v) public {
        // Update the value at this address
        console.log("set.address: ",_addr);
        console.log("set.i: ",v);
        myMapStr[_addr] = v;
    }

    function removeStr(address _addr) public {
        // Reset the value to the default value.
        console.log("set.address: ",_addr);
        delete myMapStr[_addr];
    }

    
    function getNested(address _addr1, uint _i) public view returns (bool) {
        // You can get values from a nested mapping
        // even when it is not initialized
        return nested[_addr1][_i];
    }

    function setNested(
        address _addr1,
        uint _i,
        bool _boo
    ) public {
        nested[_addr1][_i] = _boo;
    }

    function removeNested(address _addr1, uint _i) public {
        delete nested[_addr1][_i];
    }

    function push(address _addr1, uint _v) public {
        // Append to array
        // This will increase the array length by 1.
        nested2[_addr1].push(_v);
    }

    function getNested2(address _addr1, uint _i) public view returns (uint) {
        // You can get values from a nested mapping
        // even when it is not initialized
        return nested2[_addr1][_i];
    }

    function getArrNested2(address _addr1) public view returns (uint[] memory) {
        // You can get values from a nested mapping
        // even when it is not initialized
        return nested2[_addr1];
    }

    function setNested2(
        address _addr1,
        uint _i,
        uint _v
    ) public {
        nested2[_addr1][_i] = _v;
    }

    function removeNested2(address _addr1, uint _i) public {
        delete nested2[_addr1][_i];
    }

}