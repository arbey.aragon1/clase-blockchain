// SPDX-License-Identifier: MIT
pragma solidity >=0.6.0 <0.9.0;
import "hardhat/console.sol";

contract ArrayRemoveByShifting {
    // [1, 2, 3] -- remove(1) --> [1, 3, 3] --> [1, 3]
    // [1, 2, 3, 4, 5, 6] -- remove(2) --> [1, 2, 4, 5, 6, 6] --> [1, 2, 4, 5, 6]
    // [1, 2, 3, 4, 5, 6] -- remove(0) --> [2, 3, 4, 5, 6, 6] --> [2, 3, 4, 5, 6]
    // [1] -- remove(0) --> [1] --> []

    uint[] public arr;

    function remove(uint _index) public {
        require(_index < arr.length, "index out of bound");

        for (uint i = _index; i < arr.length - 1; i++) {
            arr[i] = arr[i + 1];
        }
        arr.pop();
    }

    function test() external {
        arr = [1, 2, 3, 4, 5];
        console.log("Arr: ",arr.length);
        remove(2);
        console.log("Arr: ",arr.length);

        console.log("---------------------------");
        for (uint i = 0; i < arr.length; i++) {
            console.log("v: ",i ,arr[i]);
        }

        // [1, 2, 4, 5]
        assert(arr[0] == 1);
        assert(arr[1] == 2);
        assert(arr[2] == 4);
        assert(arr[3] == 5);
        assert(arr.length == 4);

        console.log("---------------------------");
        for (uint i = 0; i < arr.length; i++) {
            console.log("v: ",i ,arr[i]);
        }

        arr = [1];

        console.log("---------------------------");
        for (uint i = 0; i < arr.length; i++) {
            console.log("v: ",i ,arr[i]);
        }

        remove(0);

        console.log("---------------------------");
        for (uint i = 0; i < arr.length; i++) {
            console.log("v: ",i ,arr[i]);
        }

        // []
        assert(arr.length == 0);
    }
}