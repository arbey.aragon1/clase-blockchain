// SPDX-License-Identifier: MIT
pragma solidity >=0.6.0 <0.9.0;
import "./StructDeclaration.sol";
import "./EnumDeclaration.sol";

contract Structs {
    // An array of 'Todo' structs
    Todo[] public todos;

    function create(string calldata _text, Status estado) public {
        // 3 ways to initialize a struct
        // - calling it like a function
        todos.push(Todo(_text, false, estado));

        // key value mapping
        todos.push(Todo({text: _text, completed: false, estado: Status.Rejected}));

        // initialize an empty struct and then update it
        Todo memory todo;
        todo.text = _text;
        // todo.completed initialized to false

        todos.push(todo);

        todos[2].text = _text;
    }

    // Solidity automatically created a getter for 'todos' so
    // you don't actually need this function.
    function get(uint _index) public view returns (string memory text, bool completed, Status estado) {
        Todo memory todo = todos[_index];
        return (todo.text, todo.completed, todo.estado);
    }

    // update text
    function updateText(uint _index, string calldata _text) public {
        Todo storage todo = todos[_index];
        todo.text = _text;
    }

    // update completed
    function toggleCompleted(uint _index) public {
        Todo storage todo = todos[_index];
        todo.completed = !todo.completed;
    }
    
    // update completed
    function statusUpdate(uint _index, Status estado) public {
        Todo storage todo = todos[_index];
        todo.estado = estado;
    }

}