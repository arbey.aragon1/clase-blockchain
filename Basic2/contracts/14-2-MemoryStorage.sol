// SPDX-License-Identifier: MIT
// compiler version must be greater than or equal to 0.8.13 and less than 0.9.0
pragma solidity >=0.6.0 <0.9.0;
import "hardhat/console.sol";

//DataLocations 
contract MemoryStorage {
    struct Persona {
        string nombre;
    }

    //array mmaping y struct 
    //porque son mas grandes
    mapping(uint=> Persona) personas;
    string public test = "";

    function getPersona() public view returns (string memory) {
        return personas[0].nombre;
    }

    function modificaStoragePersona(string memory nombre) public {
        //console.log("1 nombre original: ", personas[0].nombre);
        Persona storage _persona = personas[0];
        _persona.nombre = nombre;
        console.log("2 nombre modificado en la variable: ", _persona.nombre);
        console.log("2 nombre en el : ", personas[0].nombre);
        
    }

    function modificaMemoryPersona(string calldata nombre) public view {
        //nombre = "";
        //console.log("1 nombre original: ", personas[0].nombre);
        Persona memory _persona = personas[0];
        _persona.nombre = nombre;
        console.log("2 nombre modificado en la variable: ", _persona.nombre);
        console.log("2 nombre en el : ", personas[0].nombre);
    }
}
