// SPDX-License-Identifier: MIT
pragma solidity >=0.6.0 <0.9.0;
import "./StructDeclaration.sol";


contract Function {
    // Functions can return multiple values.
    function returnMany()
        public
        pure
        returns (
            uint,
            bool,
            uint
        )
    {
        return (1, true, 2);
    }

    // Return values can be named.
    function named()
        public
        pure
        returns (
            uint x,
            bool b,
            uint y
        )
    {
        return (1, true, 2);
    }

    // Return values can be assigned to their name.
    // In this case the return statement can be omitted.
    function assigned()
        public
        pure
        returns (
            uint x,
            bool b,
            uint y
        )
    {
        x = 1;
        b = true;
        y = 2;
    }

    // Use destructuring assignment when calling another
    // function that returns multiple values.
    function destructuringAssignments()
        public
        pure
        returns (
            uint,
            bool,
            uint,
            uint,
            uint
        )
    {
        (uint i, bool b, uint j) = returnMany();

        // Values can be left out.
        (uint x, , uint y) = (4, 5, 6);

        return (i, b, j, x, y);
    }

    // Cannot use map for either input or output

    // Can use array for input
    function arrayInput(uint[] memory _arr) public {}
    //Revisar Ejemplo en structs

    // Can use array for output
    uint[] public arr;

    function arrayOutputTest() public view returns (uint[] memory) {
        return arr;
    }
    
    Todo[] public todos;

    // Can use array for output
    function add(Todo[] memory _array) public {
        for(uint i=0; i<_array.length; i++){
            todos.push(_array[i]);
        } 
    }
    
    // Can use array for output
    function arrayOutput() public view returns (Todo[] memory) {
        return todos;
    }
}
