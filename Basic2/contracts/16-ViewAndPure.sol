// SPDX-License-Identifier: MIT
pragma solidity >=0.6.0 <0.9.0;

contract ViewAndPure {
    uint public x = 1;

    // No puede cambiar variables de estado pero si las puede leer
    function addToX(uint y) public view returns (uint) {
        //x = y;
        return x + y;
    }

    // No puede ni leer y modificar variables de estado
    function add(uint i, uint j) public pure returns (uint) {
        //x = j;
        return i + j;// + x;
    }
}