
// SPDX-License-Identifier: MIT
pragma solidity >=0.6.0 <0.9.0;
import "hardhat/console.sol";

contract Counter {
    uint public count;

    // Function to get the current count
    function get() public view returns (uint) {
        console.log("get", count);
        return count;
    }

    // Function to increment count by 1
    function inc() public {
        console.log("inc", count);
        count += 1;
        console.log("inc", count);
    }

    // Function to decrement count by 1
    function dec() public {
        // This function will fail if count = 0
        console.log("dec: ", count);
        count -= 1;
        console.log("dec: ", count);
    }
}