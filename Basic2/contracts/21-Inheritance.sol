// SPDX-License-Identifier: MIT
pragma solidity >=0.6.0 <0.9.0;

/* Graph of inheritance
    A
   / \
  B   C
 / \ /
F  D,E
*/

contract Inheritance_A {
    function foo() public pure virtual returns (string memory) {
        return "A";
    }
}

// Contracts inherit other contracts by using the keyword 'is'.
contract Inheritance_B is Inheritance_A {
    // Override A.foo()
    function foo2() public pure virtual returns (string memory) {
        return "B";
    }
    
    // Override A.foo()
    function foo() public pure virtual override returns (string memory) {
        return "B";
    }
}

contract Inheritance_C is Inheritance_A {
    // Override A.foo()
    function foo() public pure virtual override returns (string memory) {
        return "C";
    }
}

// Contracts can inherit from multiple parent contracts.
// When a function is called that is defined multiple times in
// different contracts, parent contracts are searched from
// right to left, and in depth-first manner.

contract Inheritance_D is Inheritance_B, Inheritance_C {
    // D.foo() returns "C"
    // since C is the right most parent contract with function foo()
    function foo() public pure override(Inheritance_B, Inheritance_C) returns (string memory) {
        //Inheritance_B.foo();//
        return super.foo();
    }
}

contract Inheritance_E is Inheritance_C, Inheritance_B {
    // E.foo() returns "B"
    // since B is the right most parent contract with function foo()
    function foo() public pure override(Inheritance_C, Inheritance_B) returns (string memory) {
        return super.foo();
    }
}

// Inheritance must be ordered from “most base-like” to “most derived”.
// Swapping the order of A and B will throw a compilation error.
contract Inheritance_F is Inheritance_A, Inheritance_B {
    function foo() public pure override(Inheritance_A, Inheritance_B) returns (string memory) {
        return super.foo();
    }
}
