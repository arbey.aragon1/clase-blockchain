// SPDX-License-Identifier: MIT
pragma solidity >=0.6.0 <0.9.0;
import "hardhat/console.sol";

contract Receiver {

    fallback() external payable {
        console.log(msg.sender);
        console.log(msg.value);
        console.log("Fallback was called");
    }

    function foo(string memory _message, uint _x) public payable returns (uint) {
        console.log(msg.sender);
        console.log(msg.value);
        console.log(_message);

        return _x + 1;
    }
}

contract Caller {
    constructor() payable {}
    //event Response(bool success, bytes data);

    // Let's imagine that contract Caller does not have the source code for the
    // contract Receiver, but we do know the address of contract Receiver and the function to call.
    function testCallFoo(address payable _addr) public payable {
        console.log('testCallFoo');
        // You can send ether and specify a custom gas amount
        (bool success, bytes memory data) = _addr.call{value: msg.value, gas: 5000}(
            abi.encodeWithSignature("foo(string,uint256)", "call foo", 123)
        );

        console.log(success);
        //console.log(data);
    }

    // Calling a function that does not exist triggers the fallback function.
    function testCallDoesNotExist(address _addr) public {
        console.log('testCallDoesNotExist');
        (bool success, bytes memory data) = _addr.call(
            abi.encodeWithSignature("doesNotExist()")
        );

        console.log(success);
        //console.log(data);
    }
}
