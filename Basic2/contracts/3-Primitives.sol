// SPDX-License-Identifier: MIT
pragma solidity >=0.6.0 <0.9.0;
import "hardhat/console.sol";

contract Primitives {
    bool public boo = true;

    /*
    uint stands for unsigned integer, meaning non negative integers
    different sizes are available
        uint8   ranges from 0 to 2 ** 8 - 1
        uint16  ranges from 0 to 2 ** 16 - 1
        ...
        uint256 ranges from 0 to 2 ** 256 - 1
    */
    uint8 public u8 = 1;
    uint public u256 = 456;
    uint public u = 123; // uint is an alias for uint256

    /*
    Negative numbers are allowed for int types.
    Like uint, different ranges are available from int8 to int256
    
    int256 ranges from -2 ** 255 to 2 ** 255 - 1
    int128 ranges from -2 ** 127 to 2 ** 127 - 1
    */
    int8 public i8 = -1;
    int public i256 = 456;
    int public i = -123; // int is same as int256

    // minimum and maximum of int
    int public minInt = type(int).min;
    int public maxInt = type(int).max;

    address public addr = 0xCA35b7d915458EF540aDe6068dFe2F44E8fa733c;

    /*
    In Solidity, the data type byte represent a sequence of bytes. 
    Solidity presents two type of bytes types :

     - fixed-sized byte arrays
     - dynamically-sized byte arrays.
     
     The term bytes in Solidity represents a dynamic array of bytes. 
     It’s a shorthand for byte[] .
    */
    bytes1 a = 0xb5; //  [10110101]
    bytes1 b = 0x56; //  [01010110]

    // Default values
    // Unassigned variables have a default value
    bool public defaultBoo; // false
    uint public defaultUint; // 0
    int public defaultInt; // 0
    address public defaultAddr; // 0x0000000000000000000000000000000000000000


    function OverFlow() public view {
        /*u8=0;//2**8-1;
        console.log("Text: ", u8);
        u8 = u8 - 1; 
        console.log("Text: ", u8);*/

        /*uint128 firstNo = 20;
        uint128 secondNo = 2;
        uint128 answer = firstNo / secondNo;
        console.log("Div: ", answer);
        

        secondNo = 3;
        answer = firstNo / secondNo;
        console.log("Div: ", answer);*/

        
        /*uint256 foo = 100;
        
        console.log("foo: ", foo);

        console.log("Mult: ", foo*foo);
        foo = 2;
        console.log("Exp: ", foo**3);
        
        uint128 firstNo = 20;
        uint128 answer = firstNo % 3 ;
        console.log("mod: ", answer);*/
        
        uint256 foo = 100;
        console.log("bool: ", boo);
        console.log("uint8: ", u8);
        console.log("uint: ", u256);
        
        /*console.log("int8: ", i8);
        console.log("int: ", i256);*/
        console.log("uint: ", u256);

        //console.log("minInt: ", minInt);
        //console.log("maxInt: ", maxInt);


        console.log("address: ", addr);
        //console.log("bytes1: ", a);
        //console.log("bytes1: ", b);

        console.log("Text: ", foo);

    }
}