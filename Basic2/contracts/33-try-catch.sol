// SPDX-License-Identifier: MIT
pragma solidity >=0.6.0 <0.9.0;
import "hardhat/console.sol";

// External contract used for try / catch examples
contract Foo33 {
    address public owner;

    constructor(address _owner) {
        require(_owner != address(0), "invalid address");
        assert(_owner != 0x0000000000000000000000000000000000000001);
        owner = _owner;
    }

    function myFunc(uint x) public pure returns (string memory) {
        require(x != 0, "require failed");
        return "my func was called";
    }
}

contract Bar33 {
    event LogBytes(bytes data);

    Foo33 public foo;

    constructor() {
        // This Foo contract is used for example of try catch with external call
        foo = new Foo33(msg.sender);
    }

    // Example of try / catch with external call
    // tryCatchExternalCall(0) => Log("external call failed")
    // tryCatchExternalCall(1) => Log("my func was called")
    function tryCatchExternalCall(uint _i) public {
        try foo.myFunc(_i) returns (string memory result) {
            console.log(result);
        } catch {
            console.log("external call failed");
        }
    }

    // Example of try / catch with contract creation
    // tryCatchNewContract(0x0000000000000000000000000000000000000000) => Log("invalid address")
    // tryCatchNewContract(0x0000000000000000000000000000000000000001) => LogBytes("")
    // tryCatchNewContract(0x0000000000000000000000000000000000000002) => Log("Foo created")
    function tryCatchNewContract(address _owner) public {
        try new Foo33(_owner) returns (Foo33 foo) {
            // you can use variable foo here
            console.log("Foo created");
        } catch Error(string memory reason) {
            // catch failing revert() and require()
            console.log(reason);
        } catch (bytes memory reason) {
            // catch failing assert()
            console.logBytes(reason);
        }
    }
}