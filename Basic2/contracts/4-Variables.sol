// SPDX-License-Identifier: MIT
pragma solidity >=0.6.0 <0.9.0;
import "hardhat/console.sol";

contract Variables {

    function doSomething() public payable  {
        console.log("block.gaslimit: ", block.gaslimit);
        console.log("block.timestamp: ", block.timestamp);
        
        //console.log("msg.data: ", msg.data);
        console.log("msg.sender: ", msg.sender);
        console.log("msg.value: ", msg.value);
        
        console.log("tx.gasprice: ", tx.gasprice);
    }
}