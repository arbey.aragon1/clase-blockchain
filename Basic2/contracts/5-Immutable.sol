// SPDX-License-Identifier: MIT
pragma solidity >=0.6.0 <0.9.0;
import "hardhat/console.sol";

contract Immutable {

    // coding convention to uppercase constant variables
    address public constant MY_ADDRESS_C = 0x777788889999AaAAbBbbCcccddDdeeeEfFFfCcCc;
    uint public constant MY_UINT_C = 123;

    // coding convention to uppercase constant variables
    address public immutable MY_ADDRESS;
    uint public immutable MY_UINT;

    constructor(uint _myUint) {
        MY_ADDRESS = msg.sender;
        MY_UINT = _myUint;
    }

    function doSomething() public payable  {
        console.log("MY_ADDRESS_C: ", MY_ADDRESS_C);
        console.log("MY_UINT_C: ", MY_UINT_C);

        console.log("MY_ADDRESS: ", MY_ADDRESS);
        console.log("MY_UINT: ", MY_UINT);
    }
}
