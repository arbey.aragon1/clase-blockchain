// SPDX-License-Identifier: MIT
pragma solidity >=0.6.0 <0.9.0;
import "hardhat/console.sol";

//Para escribir o actualizar una variable de estado, debe enviar una transacción.
//Por otro lado, puede leer variables de estado, de forma gratuita, sin ninguna tarifa de transacción.
contract SimpleStorage {
    // State variable to store a number
    uint public num;

    // You need to send a transaction to write to a state variable.
    function set(uint _num) public {
        num = _num;
        console.log("set num: ", num);
    }

    // You can read from a state variable without sending a transaction.
    function get() public view returns (uint) {
        console.log("get num: ", num);
        return num;
    }
}