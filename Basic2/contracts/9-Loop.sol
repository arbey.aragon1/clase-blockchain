// SPDX-License-Identifier: MIT
pragma solidity >=0.6.0 <0.9.0;
import "hardhat/console.sol";


contract Loop {
    function loop() public view {
        // for loop
        for (uint i = 0; i < 10; i++) {
            if (i == 3) {
                // Skip to next iteration with continue
                console.log("continue ",i);
                continue;
            }
            console.log(i);
            if (i == 5) {
                // Exit loop with break
                console.log("break ",i);
                break;
            }
        }

        // while loop
        uint j;
        while (j < 10) {
            console.log("while ",j);
            j++;
        }
    }
}
