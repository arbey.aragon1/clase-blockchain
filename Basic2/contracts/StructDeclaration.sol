// SPDX-License-Identifier: MIT
pragma solidity >=0.6.0 <0.9.0;
import "./EnumDeclaration.sol";
// This is saved 'StructDeclaration.sol'

struct Todo {
    string text;
    bool completed;
    Status estado;
}
