const hre = require("hardhat");

const contractName = "Mapping";

async function main() {
    const Contract = await hre.ethers.getContractFactory(contractName);
    const contract = await Contract.deploy();

    await contract.deployed();

    console.log("Counter deployed to:", contract.address);
    await run(contract.address)
}


async function run(address) {
    const MyContract = await hre.ethers.getContractFactory(contractName)
    const my_contract = await MyContract.attach(address)
    const [owner, addr1, addr2] = await hre.ethers.getSigners();
    
    await my_contract.set(addr1.address, 1);
    await my_contract.set(addr2.address, 5);
    console.log(await my_contract.get(addr1.address));
    await my_contract.remove(addr1.address);
    console.log(await my_contract.get(addr2.address));
    console.log(await my_contract.get(addr1.address));
    await my_contract.remove(addr2.address);
    console.log(await my_contract.get(addr2.address));
    console.log(await my_contract.get(addr1.address));
    console.log("*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*");


    await my_contract.setStr(addr1.address, "1");
    await my_contract.setStr(addr2.address, "5");
    console.log(await my_contract.getStr(addr1.address));
    await my_contract.removeStr(addr1.address);
    console.log(await my_contract.getStr(addr2.address));
    console.log(await my_contract.getStr(addr1.address));
    await my_contract.removeStr(addr2.address);
    console.log(await my_contract.getStr(addr2.address));
    console.log(await my_contract.getStr(addr1.address));

    
    console.log("*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*");

    await my_contract.push(addr1.address, 5);
    await my_contract.push(addr1.address, 4);
    await my_contract.push(addr1.address, 3);
    
    await my_contract.push(addr2.address, 5);
    await my_contract.push(addr2.address, 4);
    await my_contract.push(addr2.address, 3);
    
    console.log(await my_contract.getArrNested2(addr1.address));
    console.log(await my_contract.getArrNested2(addr2.address));

    console.log("*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-*-**-*-*-*");

}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
