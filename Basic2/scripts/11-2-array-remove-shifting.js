const hre = require("hardhat");

const contractName = "ArrayRemoveByShifting";

async function main() {
    const Contract = await hre.ethers.getContractFactory(contractName);
    const contract = await Contract.deploy();

    await contract.deployed();

    console.log("Counter deployed to:", contract.address);
    await run(contract.address)
}


async function run(address) {
    const MyContract = await hre.ethers.getContractFactory(contractName)
    const my_contract = await MyContract.attach(address)

    //console.log(await my_contract.get(0));
    console.log(await my_contract.test());
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
