const hre = require("hardhat");

const contractName = "Array";

async function main() {
    const Contract = await hre.ethers.getContractFactory(contractName);
    const contract = await Contract.deploy();

    await contract.deployed();

    console.log("Counter deployed to:", contract.address);
    await run(contract.address)
}


async function run(address) {
    const MyContract = await hre.ethers.getContractFactory(contractName)
    const my_contract = await MyContract.attach(address)

    //console.log(await my_contract.get(0));
    console.log(await my_contract.getArr());
    await my_contract.push(5);
    await my_contract.push(6);
    await my_contract.push(4);
    await my_contract.push(1);
    await my_contract.push(7);
    console.log(await my_contract.getArr());
    console.log("Pop: ", await my_contract.pop());
    console.log(await my_contract.getArr());
    console.log(await my_contract.getLength());
    await my_contract.remove(1);
    console.log(await my_contract.getArr());
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
