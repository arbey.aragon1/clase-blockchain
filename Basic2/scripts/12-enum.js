const hre = require("hardhat");

async function main() {
  const Counter = await hre.ethers.getContractFactory("Enum");
  const counter = await Counter.deploy();

  await counter.deployed();

  console.log("Counter deployed to:", counter.address);
  await run(counter.address)
}

async function run(address) {
  const MyContract = await hre.ethers.getContractFactory("Enum")
  const my_contract = await MyContract.attach(address)
  
  console.log("Get : ",await my_contract.get())
  await my_contract.set(3)
  console.log("set : ",await my_contract.get())
  await my_contract.cancel()
  console.log("cancel : ",await my_contract.get())
  await my_contract.reset()
  console.log("reset : ",await my_contract.get())
  
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
