const hre = require("hardhat");

const contractName = "Structs";

async function main() {
    const Contract = await hre.ethers.getContractFactory(contractName);
    const contract = await Contract.deploy();

    await contract.deployed();

    console.log("Counter deployed to:", contract.address);
    await run(contract.address)
}


async function run(address) {
    const MyContract = await hre.ethers.getContractFactory(contractName)
    const my_contract = await MyContract.attach(address)
    await my_contract.create("Santiago", 4)
    console.log("0: ",await my_contract.get(0))
    console.log("1: ",await my_contract.get(1))
    console.log("2: ",await my_contract.get(2))
    console.log()
    await my_contract.updateText(1, "Sofia")
    console.log("0: ",await my_contract.get(0))
    console.log("1: ",await my_contract.get(1))
    console.log("2: ",await my_contract.get(2))
    console.log()
    await my_contract.toggleCompleted(2)
    console.log("0: ",await my_contract.get(0))
    console.log("1: ",await my_contract.get(1))
    console.log("2: ",await my_contract.get(2))
    console.log()
    await my_contract.statusUpdate(2,2)
    console.log("0: ",await my_contract.get(0))
    console.log("1: ",await my_contract.get(1))
    console.log("2: ",await my_contract.get(2))
    console.log()
    
    console.log("2: ",(await my_contract.get(2))[0])
    console.log("2: ",(await my_contract.get(2))[1])
    console.log("2: ",(await my_contract.get(2))[2])
    console.log()
    
    console.log("2: ",(await my_contract.get(2))["text"])
    console.log("2: ",(await my_contract.get(2))['completed'])
    console.log("2: ",(await my_contract.get(2))['estado'])
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
