const hre = require("hardhat");

async function main() {
  const Counter = await hre.ethers.getContractFactory("MemoryStorage");
  const counter = await Counter.deploy();

  await counter.deployed();

  console.log("MemoryStorage deployed to:", counter.address);
  await run(counter.address)
}

async function run(address) {
  const MyContract = await hre.ethers.getContractFactory("MemoryStorage")
  const my_contract = await MyContract.attach(address)
  
  await my_contract.modificaStoragePersona("T est 1")
  //await my_contract.modificaStoragePersona("Test 2")
  console.log(await my_contract.getPersona());
  
  await my_contract.modificaMemoryPersona("Test a")
  //await my_contract.modificaMemoryPersona("Test b")
  console.log(await my_contract.getPersona());
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
