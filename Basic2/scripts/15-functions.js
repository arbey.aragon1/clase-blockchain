const hre = require("hardhat");

const contractName = "Function";

async function main() {
    const Contract = await hre.ethers.getContractFactory(contractName);
    const contract = await Contract.deploy();

    await contract.deployed();

    console.log("Counter deployed to:", contract.address);
    await run(contract.address)
}


async function run(address) {
    const MyContract = await hre.ethers.getContractFactory(contractName)
    const my_contract = await MyContract.attach(address)
    await my_contract.add([["Santiago",false,0],["Valentina",true,1]])
    console.log("Array: ",await my_contract.arrayOutput())
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
