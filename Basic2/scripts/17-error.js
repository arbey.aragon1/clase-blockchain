
const hre = require("hardhat");

const contractName = "Error";

async function main() {
    const Contract = await hre.ethers.getContractFactory(contractName);
    const contract = await Contract.deploy();

    await contract.deployed();

    console.log("Counter deployed to:", contract.address);
    await run(contract.address)
}


async function run(address) {
    const MyContract = await hre.ethers.getContractFactory(contractName)
    const my_contract = await MyContract.attach(address);

    /*console.log("testRequire: ", 20);
    await my_contract.testRequire(20);
    //console.log("testRequire: ", 1);
    //await my_contract.testRequire(1);
    
    console.log("testRevert: ", 20);
    await my_contract.testRevert(20);
    //console.log("testRevert: ", 1);
    //await my_contract.testRevert(1);*/
    
    console.log("testAssert: ");
    await my_contract.testAssert();

    console.log("testCustomError: ",0);
    await my_contract.testCustomError(0);
    console.log("testCustomError: ",1);
    await my_contract.testCustomError(1);
    

    console.log("----------");
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
