
const hre = require("hardhat");

const contractName = "FunctionModifier";

async function main() {
    const Contract = await hre.ethers.getContractFactory(contractName);
    const contract = await Contract.deploy();

    await contract.deployed();

    console.log("Counter deployed to:", contract.address);
    await run(contract.address)
}


async function run(address) {
    const MyContract = await hre.ethers.getContractFactory(contractName)
    const my_contract = await MyContract.attach(address);

    [owner, addr1, addr2, addr3, ...addrs] = await hre.ethers.getSigners();

    await my_contract.connect(owner).changeOwner(addr1.address);
    
    //await my_contract.connect(addr1).changeOwner(addr2.address);
    //await my_contract.connect(addr2).changeOwner(addr3.address);
    
    //await my_contract.connect(addr1).changeOwner(addr2.address);
    
    await my_contract.connect(addr1).decrement(10);
    await my_contract.connect(addr1).decrement(10);
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
