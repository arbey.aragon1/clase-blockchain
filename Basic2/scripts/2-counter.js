const hre = require("hardhat");

async function main() {
  const Counter = await hre.ethers.getContractFactory("Counter");
  const counter = await Counter.deploy();

  await counter.deployed();

  console.log("Counter deployed to:", counter.address);
  await run(counter.address)
}

async function run(address) {
  const MyContract = await hre.ethers.getContractFactory("Counter")
  const my_contract = await MyContract.attach(address)
  console.log(await my_contract.get())
  await my_contract.inc()
  console.log(await my_contract.get())
  await my_contract.dec()
  console.log(await my_contract.get())
  //await my_contract.dec()
  //console.log(await my_contract.get())
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.error(error);
    process.exit(1);
  });
