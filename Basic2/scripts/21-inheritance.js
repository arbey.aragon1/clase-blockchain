const hre = require("hardhat");


async function deployContract(cName) {
    const Contract = await hre.ethers.getContractFactory(cName);
    const contract = await Contract.deploy();
    await contract.deployed();
    console.log("Deploy: ", cName);
    return contract;
}


async function main() {
    const d = await deployContract("Inheritance_D");
    const e = await deployContract("Inheritance_E");
    const f = await deployContract("Inheritance_F");

    console.log("D: ", await d.foo());
    console.log("E: ", await e.foo());
    console.log("F: ", await f.foo());

    
    //const shadowing_C = await deployContract("Shadowing_C");
    //console.log("Shadowing_C: ", await shadowing_C.getName());
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
