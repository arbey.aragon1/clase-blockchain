
const hre = require("hardhat");


async function deployContract(cName) {
    const Contract = await hre.ethers.getContractFactory(cName);
    const contract = await Contract.deploy();
    await contract.deployed();
    console.log("Deploy: ", cName);
    return contract;
}

async function main() {
    const shadowing_A = await deployContract("Shadowing_A");
    const shadowing_C = await deployContract("Shadowing_C");

    console.log("A: ", await shadowing_A.getName());
    console.log("C: ", await shadowing_C.getName());
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
