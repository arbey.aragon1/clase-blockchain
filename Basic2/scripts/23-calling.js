
const hre = require("hardhat");


async function deployContract(cName) {
    const Contract = await hre.ethers.getContractFactory(cName);
    const contract = await Contract.deploy();
    await contract.deployed();
    console.log("Deploy: ", cName);
    return contract;
}


async function main() {
    const Call_A = await deployContract("Call_A");
    const Call_B = await deployContract("Call_B");
    const Call_C = await deployContract("Call_C");
    const Call_D = await deployContract("Call_D");

    console.log();
    console.log("Call_A.foo: " );
    await Call_A.foo()
    console.log("Call_A.bar: " );
    await Call_A.bar()
    
    console.log();
    console.log("Call_B.foo: " );
    await Call_B.foo()
    console.log("Call_B.bar: " );
    await Call_B.bar()
    
    console.log();
    console.log("Call_C.foo: " );
    await Call_C.foo()
    console.log("Call_C.bar: " );
    await Call_C.bar()

    console.log();
    console.log("Call_D.foo: " );
    await Call_D.foo()
    console.log("Call_D.bar: " );
    await Call_D.bar()
    
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
