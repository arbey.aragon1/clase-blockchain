const hre = require("hardhat");


async function deployContract(cName) {
    const Contract = await hre.ethers.getContractFactory(cName);
    const contract = await Contract.deploy();
    await contract.deployed();
    console.log("Deploy: ", cName);
    return contract;
}


async function main() {
    const Visibility = await deployContract("Visibility");
    const VisibilityChild = await deployContract("VisibilityChild");

    //console.log("Visibility.privateFunc: ", await Visibility.privateFunc());
    
    //console.log("Visibility.testPrivateFunc: ", await Visibility.testPrivateFunc());
    //console.log("Visibility.internalFunc: ", await Visibility.internalFunc());
    
    console.log("VisibilityChild.internalFunc: ", await VisibilityChild.externalFunc());
    

    //console.log("Visibility.testInternalFunc: ", await Visibility.testInternalFunc());
    //console.log("Visibility.publicFunc: ", await Visibility.publicFunc());
    //console.log("Visibility.externalFunc: ", await Visibility.externalFunc());
    //console.log("VisibilityChild.testInternalFunc: ", await VisibilityChild.testInternalFunc());
    //
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
