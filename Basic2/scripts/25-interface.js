const hre = require("hardhat");


async function deployContract(addr, cName) {
    const Contract = await hre.ethers.getContractFactory(cName);
    const contract = await Contract.connect(addr).deploy();
    await contract.deployed();
    console.log("Deploy: ", cName);
    return contract;
}


async function main() {
    [owner, addr1, addr2, ...addrs] = await hre.ethers.getSigners();

    const MyCounter = await deployContract(addr1, "MyCounter");
    const MyContract = await deployContract(owner, "MyContract");
    
    //Traemos el valor del contrato
    console.log(await MyCounter.connect(addr1).get());

    //hacemos llamada del primer contrato
    await MyContract.connect(owner).incrementCounter(MyCounter.address);
    
    //Traemos el valor del contrato
    console.log(await MyCounter.connect(addr1).get());

    
    //const UniswapExample = await deployContract(owner, "UniswapExample");
    //console.log(await UniswapExample.getTokenReserves());
    
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
