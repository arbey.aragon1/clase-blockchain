
//const hre = require("hardhat");
const { ethers, waffle} = require("hardhat");
//const provider = waffle.provider;

async function deployContract(cName, initialBalance ) {
    const Contract = await ethers.getContractFactory(cName);
    const contract = await Contract.deploy({ value: initialBalance });
    await contract.deployed();
    console.log("Deploy: ", cName);
    return contract;
}


async function main() {
    [owner, addr1, addr2, ...addrs] = await ethers.getSigners();

    const initialBalance = ethers.utils.parseEther("1");
    const Payable = await deployContract("PayableContract", initialBalance );

    console.log("-------------------------------------------------------------------------------");
    console.log('Owner balance: ', await ethers.provider.getBalance(owner.address));
    console.log('Contract balance: ', await ethers.provider.getBalance(Payable.address));
    console.log("-------------------------------------------------------------------------------");
    console.log(ethers.utils.parseEther("1.0"));
    console.log("-------------------------------------------------------------------------------");
    await Payable.deposit({
        from: owner.address,
        value: ethers.utils.parseEther("1.0")
    });

    
    /*await Payable.notPayable({
        from: owner.address,
        value: ethers.utils.parseEther("1.0")
    });*/

    console.log("-------------------------------------------------------------------------------");
    console.log('Contract balance: ', await ethers.provider.getBalance(Payable.address));
    console.log("-------------------------------------------------------------------------------");
    await Payable.withdraw();
    
    console.log("-------------------------------------------------------------------------------");
    console.log('Owner balance: ', await ethers.provider.getBalance(owner.address));
    console.log('Contract balance: ', await ethers.provider.getBalance(Payable.address));
    console.log("-------------------------------------------------------------------------------");


    await Payable.deposit({
        from: owner.address,
        value: ethers.utils.parseEther("1.0")
    });
    
    console.log('other balance: ', await ethers.provider.getBalance(addr1.address));
    console.log(await ethers.provider.getBalance(Payable.address));

    await Payable.transfer(addr1.address, ethers.utils.parseEther("1.0"));

    console.log('other balance: ', await ethers.provider.getBalance(addr1.address));

}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
