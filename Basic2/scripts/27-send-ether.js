
//const hre = require("hardhat");
const { ethers, waffle} = require("hardhat");
//const provider = waffle.provider;

async function deployContract(cName) {
    const Contract = await ethers.getContractFactory(cName);
    const contract = await Contract.deploy();
    await contract.deployed();
    console.log("Deploy: ", cName);
    return contract;
}

async function deployInitContract(cName, initialBalance ) {
    const Contract = await ethers.getContractFactory(cName);
    const contract = await Contract.deploy({ value: initialBalance });
    await contract.deployed();
    console.log("Deploy: ", cName);
    return contract;
}


async function main() {
    [owner, addr1, addr2, ...addrs] = await ethers.getSigners();

    const initialBalance = ethers.utils.parseEther("5");
    const SendEther = await deployInitContract("SendEther", initialBalance);
    const ReceiveEther = await deployContract("ReceiveEther" );

    console.log('SendEther balance: ', await ethers.provider.getBalance(SendEther.address));
    console.log('ReceiveEther balance: ', await ethers.provider.getBalance(ReceiveEther.address));
    
    await addr1.sendTransaction({
        to: ReceiveEther.address,
        value: ethers.utils.parseUnits('1')
    });
        

    console.log('SendEther balance: ', await ethers.provider.getBalance(SendEther.address));
    console.log('ReceiveEther balance: ', await ethers.provider.getBalance(ReceiveEther.address));


    //await SendEther.sendViaTransfer(ReceiveEther.address, { value: ethers.utils.parseEther("1") });
    //await SendEther.sendViaSend(ReceiveEther.address, { value: ethers.utils.parseEther("1") });
    await SendEther.sendViaCall(ReceiveEther.address, { value: ethers.utils.parseEther("1") });

    
    console.log('SendEther balance: ', await ethers.provider.getBalance(SendEther.address));
    console.log('ReceiveEther balance: ', await ethers.provider.getBalance(ReceiveEther.address));

}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
