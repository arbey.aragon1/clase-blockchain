
//const hre = require("hardhat");
const { ethers, waffle} = require("hardhat");
//const provider = waffle.provider;

async function deployContract(cName) {
    const Contract = await ethers.getContractFactory(cName);
    const contract = await Contract.deploy();
    await contract.deployed();
    console.log("Deploy: ", cName);
    return contract;
}

async function deployInitContract(cName, initialBalance ) {
    const Contract = await ethers.getContractFactory(cName);
    const contract = await Contract.deploy({ value: initialBalance });
    await contract.deployed();
    console.log("Deploy: ", cName);
    return contract;
}


async function main() {
    [owner, addr1, addr2, ...addrs] = await ethers.getSigners();

    const initialBalance = ethers.utils.parseEther("5");

    const Fallback = await deployContract("Fallback" );
    const SendToFallback = await deployInitContract("SendToFallback", initialBalance);

    console.log("-------------------------------------------------------------");
    console.log('SendToFallback balance: ', await ethers.provider.getBalance(SendToFallback.address));
    console.log('Fallback balance: ', await ethers.provider.getBalance(Fallback.address));
    console.log("-------------------------------------------------------------");
    
    //await SendToFallback.transferToFallback(Fallback.address,{value: ethers.utils.parseUnits('1')});
    await SendToFallback.callFallback(Fallback.address,{value: ethers.utils.parseUnits('1')});
    
    console.log("-------------------------------------------------------------");
    console.log('SendToFallback balance: ', await ethers.provider.getBalance(SendToFallback.address));
    console.log('Fallback balance: ', await ethers.provider.getBalance(Fallback.address));
    console.log("-------------------------------------------------------------");
    
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
