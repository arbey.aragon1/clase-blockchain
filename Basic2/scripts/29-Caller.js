
//const hre = require("hardhat");
const { ethers, waffle} = require("hardhat");
//const provider = waffle.provider;

async function deployContract(cName) {
    const Contract = await ethers.getContractFactory(cName);
    const contract = await Contract.deploy();
    await contract.deployed();
    console.log("Deploy: ", cName);
    return contract;
}

async function deployInitContract(cName, initialBalance ) {
    const Contract = await ethers.getContractFactory(cName);
    const contract = await Contract.deploy({ value: initialBalance });
    await contract.deployed();
    console.log("Deploy: ", cName);
    return contract;
}


async function main() {
    [owner, addr1, addr2, ...addrs] = await ethers.getSigners();

    const initialBalance = ethers.utils.parseEther("5");

    const Receiver = await deployContract("Receiver" );
    const Caller = await deployInitContract("Caller", initialBalance);

    console.log("-------------------------------------------------------------");
    console.log('Receiver balance: ', await ethers.provider.getBalance(Receiver.address));
    console.log('Caller balance: ', await ethers.provider.getBalance(Caller.address));
    
    console.log("-------------------------------------------------------------");
    await Caller.testCallFoo(Receiver.address,{value: ethers.utils.parseUnits('1')});
    console.log("-------------------------------------------------------------");
    await Caller.testCallDoesNotExist(Receiver.address);

    console.log("-------------------------------------------------------------");
    console.log('Receiver balance: ', await ethers.provider.getBalance(Receiver.address));
    console.log('Caller balance: ', await ethers.provider.getBalance(Caller.address));
    console.log("-------------------------------------------------------------");
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
