
//const hre = require("hardhat");
const { ethers, waffle} = require("hardhat");
//const provider = waffle.provider;

async function deployContract(cName) {
    const Contract = await ethers.getContractFactory(cName);
    const contract = await Contract.deploy();
    await contract.deployed();
    console.log("Deploy: ", cName);
    return contract;
}



async function main() {
    [owner, addr1, addr2, ...addrs] = await ethers.getSigners();

    const A = await deployContract("DelegatecallA" );
    const B = await deployContract("DelegatecallB" );

    console.log("-------------------------------------------------------------");
    console.log('A v: ', await A.get());
    console.log('B v: ', await B.get());
    await A.setVars(B.address, 5);
    console.log('A v: ', await A.get());
    console.log('B v: ', await B.get());
    console.log("-------------------------------------------------------------");
    
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
