
//const hre = require("hardhat");
const { ethers, waffle} = require("hardhat");
//const provider = waffle.provider;

async function deployContract(cName) {
    const Contract = await ethers.getContractFactory(cName);
    const contract = await Contract.deploy();
    await contract.deployed();
    console.log("Deploy: ", cName);
    return contract;
}



async function main() {
    [owner, addr1, addr2, ...addrs] = await ethers.getSigners();

    const Caller2 = await deployContract("Caller2");
    const Callee = await deployContract("Callee");

    console.log("-------------------------------------------------------------");
    await Caller2.setXFromAddress(Callee.address, 5);
    await Caller2.setX(Callee.address, 2);
    await Caller2.setXandSendEther(Callee.address, 1);

}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
