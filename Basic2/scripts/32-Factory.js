
//const hre = require("hardhat");
const { ethers, waffle} = require("hardhat");
//const provider = waffle.provider;

async function deployContract(cName) {
    const Contract = await ethers.getContractFactory(cName);
    const contract = await Contract.deploy();
    await contract.deployed();
    console.log("Deploy: ", cName);
    return contract;
}



async function main() {
    [owner, addr1, addr2, addr3, addr4, addr5, ...addrs] = await ethers.getSigners();

    const CarFactory = await deployContract("CarFactory");


    const initialBalance = ethers.utils.parseEther("0.5");
    console.log("-------------------------------------------------------------");
    await CarFactory.create(addr1.address, "Ferrary");
    await CarFactory.createAndSendEther(addr2.address, "toyota", {value: initialBalance});

    //const salt = ethers.utils.keccak256(0x0);
    //await CarFactory.create2(addr3.address, "Mercedes", {salt: salt});
    //await CarFactory.create2AndSendEther(addr4.address, "Lamborgini", {value: initialBalance, salt: salt});

    
    console.log("-------------------------------------------------------------");
    console.log(await CarFactory.getCar(0));
    console.log(await CarFactory.getCar(1));
    //console.log(await CarFactory.getCar(2));
    //console.log(await CarFactory.getCar(3));
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
