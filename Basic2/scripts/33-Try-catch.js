
//const hre = require("hardhat");
const { ethers, waffle} = require("hardhat");
//const provider = waffle.provider;

async function deployContract(cName) {
    const Contract = await ethers.getContractFactory(cName);
    const contract = await Contract.deploy();
    await contract.deployed();
    console.log("Deploy: ", cName);
    return contract;
}



async function main() {
    [owner, addr1, addr2, addr3, addr4, addr5, ...addrs] = await ethers.getSigners();

    const Bar33 = await deployContract("Bar33");

    console.log("-------------------------------------------------------------");
    await Bar33.tryCatchExternalCall(1);
    await Bar33.tryCatchExternalCall(0);
    
    console.log("-------------------------------------------------------------");
    // tryCatchNewContract(0x0000000000000000000000000000000000000002) => Log("Foo created")
    await Bar33.tryCatchNewContract('0x0000000000000000000000000000000000000002');
    
    // tryCatchNewContract(0x0000000000000000000000000000000000000001) => LogBytes("")
    await Bar33.tryCatchNewContract('0x0000000000000000000000000000000000000001');
    
    // tryCatchNewContract(0x0000000000000000000000000000000000000000) => Log("invalid address")
    await Bar33.tryCatchNewContract('0x0000000000000000000000000000000000000000');
 
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
