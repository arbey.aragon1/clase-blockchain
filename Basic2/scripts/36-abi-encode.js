
//const hre = require("hardhat");
const { ethers, waffle} = require("hardhat");
//const provider = waffle.provider;

async function deployContract(cName) {
    const Contract = await ethers.getContractFactory(cName);
    const contract = await Contract.deploy();
    await contract.deployed();
    console.log("Deploy: ", cName);
    return contract;
}



async function main() {
    [owner, addr1, addr2, addr3, addr4, addr5, ...addrs] = await ethers.getSigners();

    const AbiEncode = await deployContract("AbiEncode");
    const Token = await deployContract("Token");

    console.log("-------------------------------------------------------------");
    const v = await AbiEncode.encodeWithSignature(addr1.address, 4)
    console.log(v);
    const c = await AbiEncode.encodeWithSelector(addr1.address, 4)
    console.log(c);

    console.log("-------------------------------------------------------------");
    await AbiEncode.test(Token.address, v);
    await AbiEncode.test(Token.address, c);
    //await TestArray.testArrayRemove();
    
 
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
