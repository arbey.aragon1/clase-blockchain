
//const hre = require("hardhat");
const { ethers, waffle} = require("hardhat");
//const provider = waffle.provider;

async function deployContract(cName) {
    const Contract = await ethers.getContractFactory(cName);
    const contract = await Contract.deploy();
    await contract.deployed();
    console.log("Deploy: ", cName);
    return contract;
}



async function main() {
    [owner, addr1, addr2, addr3, addr4, addr5, ...addrs] = await ethers.getSigners();

    const AbiDecode = await deployContract("AbiDecode");

    console.log("-------------------------------------------------------------");
    const v = await AbiDecode.encode(5, addr1.address, [1,2,3], {'name': 'test', 'nums': [1,2]})
    console.log(v);

    console.log("-------------------------------------------------------------");
    await AbiDecode.decode(v);
    //await AbiEncode.test(Token.address, c);
    //await TestArray.testArrayRemove();
    
 
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
