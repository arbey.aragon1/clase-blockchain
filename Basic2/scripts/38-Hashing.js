
//const hre = require("hardhat");
const { ethers, waffle} = require("hardhat");
//const provider = waffle.provider;

async function deployContract(cName) {
    const Contract = await ethers.getContractFactory(cName);
    const contract = await Contract.deploy();
    await contract.deployed();
    console.log("Deploy: ", cName);
    return contract;
}



async function main() {
    [owner, addr1, addr2, addr3, addr4, addr5, ...addrs] = await ethers.getSigners();

    const HashFunction = await deployContract("HashFunction");
    const GuessTheMagicWord = await deployContract("GuessTheMagicWord");

    console.log("-------------------------------------------------------------");
    const v = await HashFunction.hash('Arbey', 5, addr1.address);
    console.log(v);
    
    console.log("-------------------------------------------------------------");
    console.log(await HashFunction.collision('AAA','BBB'));
    console.log(await HashFunction.collision('AA','ABBB'));
    
    console.log("-------------------------------------------------------------");
    console.log(await GuessTheMagicWord.guess('Solidity'));
    //await AbiEncode.test(Token.address, c);
    //await TestArray.testArrayRemove();
    
 
}

main()
    .then(() => process.exit(0))
    .catch((error) => {
        console.error(error);
        process.exit(1);
    });
